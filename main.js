const btn = document.querySelector(".btn");

btn.addEventListener("click", getIp);

async function getIp() {
  try {
    const request = await fetch("https://api.ipify.org/?format=json");
    const data = await request.json();
    console.log(data);

    const physicalAdress = await getPhysicAdress(data.ip);
    render(physicalAdress);
  } catch (e) {
    console.error("Error fetching IP or physical address:", e);
  }
}

async function getPhysicAdress(ip) {
  try {
    const request = await fetch(`http://ip-api.com/json/${ip}`);
    const data = await request.json();
    console.log(data);
    return data;
  } catch (e) {
    console.error("Error fetching physical address:", e);
  }
}
function render(data) {
  console.log(data);
  const continent = document.createElement("p");
  const county = document.createElement("p");
  const region = document.createElement("p");
  const city = document.createElement("p");
  const area = document.createElement("p");

  continent.textContent = `Континент: ${data.timezone.split("/")[0]}`;
  county.textContent = `Країна: ${data.country}`;
  region.textContent = `Регіон: ${data.region}`;
  city.textContent = `Місто: ${data.city}`;
  area.textContent = `Район: ${data.regionName}`;

  document.body.append(continent, county, region, city, area);
}
